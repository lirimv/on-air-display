import React, {Component} from 'react';
import image from '../images/on-air-gif-1.gif'

class Status extends Component {

    constructor(props) {
        super(props);
        this.state = {
            error: null,
            isLoaded: false,
            status: false,
            delay: 3000
        };
    }

    async componentDidMount() {
        setInterval( async () => {
            await fetch("http://on-air-display/api")
                .then(res => res.json())
                .then(
                    (result) => {
                        this.setState({
                            isLoaded: true,
                            status: result.status === 'on'
                        });
                    },
                    // Note: it's important to handle errors here
                    // instead of a catch() block so that we don't swallow
                    // exceptions from actual bugs in components.
                    (error) => {
                        this.setState({
                            isLoaded: true,
                            error
                        });
                })
            }, this.state.delay

        )
    }

    render() {
        const {error, isLoaded, status} = this.state;
        if (error) {
            return <div>Error: {error.message}</div>;
        } else if (!isLoaded) {
            return <div>Loading...</div>;
        } else {

            if (status === true) {
                return (<img width={"100%"} src={image} alt='on air'/>)
            }

            return (<h1>easy</h1>)
        }
    }
}

export default Status;