<?php
declare(strict_types=1);

namespace App\Controller;

use JsonException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/api")
 */
final class ApiController
{
    private const DATA_FILE = __DIR__ . '/../../data/status.json';

    /**
     * @Route("", methods={"GET"})
     */
    public function getAction(): JsonResponse
    {
        $data = file_get_contents(self::DATA_FILE);

        try {
            $response = json_decode($data, true, 512, JSON_THROW_ON_ERROR);
        } catch (JsonException $e) {
            return new JsonResponse(null, 500);
        }

        return new JsonResponse($response, 200);
    }

    /**
     * @Route("/on", methods={"PUT"})
     */
    public function on(): Response
    {
        $this->update(true);

        return new JsonResponse(null, 204);
    }

    /**
     * @Route("/off", methods={"PUT"})
     */
    public function off(): Response
    {
        $this->update(false);

        return new JsonResponse(null, 204);
    }

    private function update(bool $status): void
    {
        try {
            $data = json_encode(['status' => $status ? 'on' : 'off'], JSON_THROW_ON_ERROR);
        } catch (JsonException $e) {
            return;
        }

        file_put_contents(self::DATA_FILE, $data);
    }
}
